import protocol
import thread
import handler

ip = "1.1.1.1"
port = 25000

def main():
    try:
        protocol.init(ip,port)
        thread.start_new_thread(protocol.receive, ())
        while True:
            try:
                for sym in ["BOND"]:
                   # if handler.N % 2 ==0:
                    protocol.send(handler.buy(sym))
                    handler.N += 1
                    # else:
                    protocol.send(handler.sell(sym))
                    handler.N += 1
                    ##
                    #protocol.send(handler.extra_buy(best_stock))
                    ##
            except:
                protocol.close()
                protocol.init(ip,port)
                print "RESET *************************************"
            pass

    except thread.error as e:
        print e
    finally:
        protocol.close()
main()
