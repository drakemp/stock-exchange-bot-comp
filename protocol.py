import simplejson as json
import socket
import handler as handler
import thread
import traceback

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_in = client.makefile("r", 1)

def init(ip, port):
    client.connect((ip,port))
    hello = {'type':'hello', 'team':'KLAURIA'}
    client.send(json.dumps(hello) + "\n")

def close():
    client.close()

def send(dict):
    client.send(json.dumps(dict)+"\n")

#***blocking call****
def receive():
    try:
        while True:
            dict = json.loads(client_in.readline())
            thread.start_new_thread(handler.handler, (dict,))

    except KeyboardInterrupt as e:
        print e
        print "Quitting..."
    finally:
        close()
