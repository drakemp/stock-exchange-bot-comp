#making decision based on the info
#helpers###
#with decision make format and send back
import threading
import numpy
#Server message handler
queue = []
queue_s = []
avg_buy = 0
avg_sell = 0
methods = ['hello', 'open', 'close', 'error', 'book', 'trade', 'ack', 'reject', 'fill', 'out']

sync = threading.Lock()
def print_debug(msg):
    sync.acquire()
    print msg
    sync.release()
    pass

def hello(dict):
    print_debug('HELLO -> symbols: {}'.format(dict['symbols']))

def open_h(dict):
    print_debug('OPEN -> symbols: {}'.format(dict['symbols']))

def close(dict):
    print_debug('CLOSE -> symbols: {}'.format(dict['symbols']))

def error(dict):
    print_debug('ERROR -> error: {}'.format(dict['error']))


def book(dict):
    global queue
    global queue_s
    global avg_buy
    global avg_sell
    #print_debug('BOOK -> symbol: {}, buy: {}, sell: {}'.format(dict['symbol'], dict['buy'], dict['sell']))
#    update_book(dict)
 #   if (dict['symbol'] == 'AAPL'):
  #      queue.extend(dict['buy'])
   # if (len(queue) >= 20):
    #    avg_buy=  sum(numpy.array(queue).flatten()) / len(queue)
     #   queue = []
#
 #   if (dict['symbol'] == 'AAPL'):
  #      queue_s.extend(dict['sell'])
   # if (len(queue_s) >= 20):
    #    avg_sell=  sum(numpy.array(queue_s).flatten()) / len(queue_s)
     #   queue_s = []

def trade(dict):
    #print_debug('TRADE -> symbol: {}, price: {}, size: {}'.format(dict['symbol'], dict['price'], dict['size']))
    pass

def ack(dict):
    print_debug('ACK -> order_id: {}'.format(dict['order_id']))

def reject(dict):
    # print_debug('REJECT -> order_id: {}, error: {}'.format(dict['order_id'], dict['error']))
    pass

def fill(dict):
    print_debug('FILL -> order_id: {}, symbol: {}, dir: {}, price: {}, size: {}' \
            .format(dict['order_id'], dict['symbol'], dict['dir'], dict['price'], dict['size']))

def out(dict):
    print_debug('OUT -> order_id: {}'.format(dict['order_id']))

methods_dict = {
    'hello':hello,
    'open':open_h,
    'close':close,
    'error':error,
    'book':book,
    'trade':trade,
    'ack':ack,
    'reject':reject,
    'fill':fill,
    'out':out
}


best_sell = {'BOND':0.0,
        'BABZ':0.0,
        'BABA':0.0,
        'AAPL':0.0,
        'MSFT':0.0,
        'GOOG':0.0,
        'XLK':0.0}
best_buy = {'BOND':0.0,
        'BABZ':0.0,
        'BABA':0.0,
        'AAPL':0.0,
        'MSFT':0.0,
        'GOOG':0.0,
        'XLK':0.0}

def handler(dict):
    if dict['type'] in methods:
        methods_dict[dict['type']](dict)
    else:
        print('bad method: {}'.format(dict['type']))

hello_cmd = {'type':'hello', 'team':'KLAURIA'}

N = 0

def buy(sym):
    #if sym == 'AAPL':
     #   return {'type':'add', 'order_id': N, 'symbol':sym, 'dir':'BUY',
      #          'price': avg_buy, 'size':2}
    return {'type':'add', 'order_id': N, 'symbol':sym, 'dir':'BUY',
            'price': best_buy['syn'], 'size':100}

def sell(sym):
  #  if sym == 'AAPL':
   #     return {'type':'add', 'order_id': N, 'symbol':sym, 'dir':'SELL',
    #            'price': avg_sell + 3, 'size':2}
    return {'type':'add', 'order_id': N, 'symbol':sym, 'dir':'SELL',
            'price': best_sell['syn'], 'size':100}
##
#def extra_buy(best_stock):
#    return
##

def update_book(message): #message is a dictionary/JSON object, idk
    if message["type"] == "book":
        sym = message["symbol"]
        buy = -1
        sell = 100000000
        for price in message["buy"]:
            if price[0] > buy:
                buy = price[0]
        for price in message["sell"]:
            if price[0] < sell:
                sell = price[0]
        best_buy[sym] = buy
        best_sell[sym] = sell
