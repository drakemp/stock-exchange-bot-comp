#Server message handler

import threading

methods = ['hello', 'open', 'close', 'error', 'book', 'trade', 'ack', 'reject', 'fill', 'out']

sync = threading.Lock()
def print_debug(msg):
    sync.acquire()
    print msg
    sync.release()

def hello(dict):
    print_debug('HELLO -> symbols: {}'.format(dict['symbols']))

def open_h(dict):
    print_debug('OPEN -> symbols: {}'.format(dict['symbols']))

def close(dict):
    print_debug('CLOSE -> symbols: {}'.format(dict['symbols']))

def error(dict):
    print_debug('ERROR -> error: {}'.format(dict['error']))

def book(dict):
    print_debug('BOOK -> symbol: {}, buy: {}, sell: {}'.format(dict['symbol'], dict['buy'], dict['sell']))
    #do something
    
def trade(dict):
    print_debug('TRADE -> symbol: {}, price: {}, size: {}'.format(dict['symbol'], dict['price'], dict['size']))

def ack(dict):
    print_debug('ACK -> order_id: {}'.format(dict['order_id']))

def reject(dict):
    print_debug('REJECT -> order_id: {}, error: {}'.format(dict['order_id'], dict['error']))

def fill(dict):
    print_debug('FILL -> order_id: {}, symbol: {}, dir: {}, price: {}, size: {}' \
          .format(dict['order_id'], dict['symbol'], dict['dir'], dict['price'], dict['size']))

def out(dict):
    print_debug('OUT -> order_id: {}'.format(dict['order_id']))

methods_dict = {
    'hello':hello,
    'open':open_h,
    'close':close,
    'error':error,
    'book':book,
    'trade':trade,
    'ack':ack,
    'reject':reject,
    'fill':fill,
    'out':out
}

def handler(dict):
#    sync.acquire()
#    print '*********************************'
#    print dict
#    print '*********************************'
#    sync.release()
    if dict['type'] in methods:
        methods_dict[dict["type"]](dict)
    else:
        print('bad method: {}'.format(dict['type']))
